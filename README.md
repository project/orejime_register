# Orejime register by Ecedi

Provides a register of individual cookies consents.

## Summary

Orejime register have been developed around the GDPR article 7.1
According to the french Data Protection Authority (the CNIL - feel free to check with your local authority) there are several ways to comply with it :

- The different versions of the computer code used by the consent-gathering organization can be sequestered by a third party, or, more simply, a condensed (or "hash") version of this code can be published in a time-stamped manner on a public platform, to be able to prove its authenticity posteriorly;
- A screenshot of the visual rendering displayed on a mobile or fixed terminal can be kept, time-stamped, for each version of the site or application;
- Regular audits of the mechanisms for collecting consent implemented by the sites or applications from which it is collected may be implemented by third parties mandated for this purpose;
- Information relating to the tools implemented and their successive configuration (such as consent collection solutions, also known as CMP, for "Consent Management Platform") may be kept, in a time-stamped manner, by the third parties publishing these solutions.

It is also possible to keep a register of individual consents as additional documentation provided that it complies with standard data protection rules: data minimization, legitimate retention period, security, etc.

## Installation and requirements

1. Install Orejime register as you would install any Drupal module.
2. There is no step 2 !

Orejime register has no configuration. It will simply save every user response to a database table and provide a list in the back-office reports section, along with a full data purge and a purge by date form.
It requires the Orejime module (https://www.drupal.org/project/orejime) developed by GAYA.

## Disclaimers

This module is in no way related to Orejime module nor with GAYA.
