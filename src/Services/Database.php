<?php

namespace Drupal\orejime_register\Services;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orejime\Entity\Orejime;

/**
 * Defines Database class.
 */
class Database extends ControllerBase {

  const TABLE_NAME = 'orejime_register__cookie_register';

  /**
   * The database connection used to store entity usage information.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Database constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database link.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Adds an entry to the registry on user action.
   *
   * @param array $data
   *   Array containing approval or refusal for each cookie.
   */
  public function addEntry(array $data) {
    $schema = $this->database->schema();
    $storage = $this->entityTypeManager->getStorage('orejime_service');

    foreach ($data as $orejimeName => $field) {
      $orejimeIds = $storage->getQuery()->accessCheck()->condition('name', $orejimeName)->execute();
      $orejimeId = array_pop($orejimeIds);
      $columnName = $orejimeName . '_' . $orejimeId;
      if ($schema->fieldExists(self::TABLE_NAME, $columnName)) {
        $data[$columnName] = $field ?: '0';
      }
      unset($data[$orejimeName]);
    }

    if (!empty($data)) {
      $data['created_at'] = date_create()->format('Y-m-d H:i:s');
      $this->database->insert(self::TABLE_NAME)->fields($data)->execute();
    }
  }

  /**
   * Creates a new column in the register table.
   *
   * @param \Drupal\orejime\Entity\Orejime $entity
   *   The content to base the column on.
   */
  public function createColumn(Orejime $entity) {
    $schema = $this->database->schema();

    if (!$schema->tableExists(self::TABLE_NAME) ||
      !$entity->hasField('name') ||
      $entity->name->isEmpty()
    ) {
      return;
    }

    $name = $entity->name->value;
    $oid = $entity->id();

    $columnName = $name . '_' . $oid;

    if (!$schema->fieldExists(self::TABLE_NAME, $columnName)) {
      $schema->addField(self::TABLE_NAME, $columnName, $this->getFieldSpec($name));
    }
  }

  /**
   * Get orejime_register table schema.
   */
  public function getSchema() {
    $cookieRegisterSchema = [
      'description' => 'Register of cookies acceptances and refusals.',
      'fields' => [
        'id' => [
          'description' => 'Auto-incremental ID.',
          'type' => 'serial',
          'not null' => TRUE,
          'unsigned' => TRUE,
        ],
        'created_at' => [
          'description' => 'Date of the entry.',
          'type' => 'varchar',
          'mysql_type' => 'datetime',
          'not null' => TRUE,
        ],
      ],
      'primary key' => [
        'id',
      ],
    ];

    try {
      $storage = $this->entityTypeManager->getStorage('orejime_service');
      $orejimeIds = $storage->getQuery()->accessCheck()->execute();
      $orejimeContents = $storage->loadMultiple($orejimeIds);
    }
    catch (PluginNotFoundException $exception) {
      $orejimeContents = [];
    }

    /** @var \Drupal\orejime\Entity\Orejime $orejimeContent */
    foreach ($orejimeContents as $orejimeContent) {
      if ($orejimeContent->hasField('name') && !$orejimeContent->name->isEmpty()) {
        $name = $orejimeContent->name->value;
        $oid = $orejimeContent->id();
        $cookieRegisterSchema['fields'][$name . '_' . $oid] = $this->getFieldSpec($name);
      }
    }

    return [self::TABLE_NAME => $cookieRegisterSchema];
  }

  /**
   * Get a list of all registry entries.
   *
   * @return array
   *   The list of all registry entries.
   */
  public function list(): array {
    return $this->database
      ->select(self::TABLE_NAME, 't')
      ->extend(PagerSelectExtender::class)
      ->element(0)
      ->fields('t')
      ->limit(20)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Delete all registry entries and update column names.
   */
  public function purge() {
    $schema = $this->database->schema();
    $schema->dropTable(self::TABLE_NAME);

    $schema->createTable(self::TABLE_NAME, $this->getSchema()[self::TABLE_NAME]);
  }

  /**
   * Deletes registry entries by date.
   */
  public function purgeByDate(\DateTime $startDate, \DateTime $endDate) {
    $this->database->delete(self::TABLE_NAME)
      ->condition('created_at', $startDate->format('Y-m-d'), '>=')
      ->condition('created_at', $endDate->modify('+1 day')->format('Y-m-d'), '<')
      ->execute();
  }

  /**
   * Updates an existing column in the register table.
   *
   * @param \Drupal\orejime\Entity\Orejime $entity
   *   The content to base the column on.
   */
  public function updateColumn(Orejime $entity) {
    $schema = $this->database->schema();

    if (!$schema->tableExists(self::TABLE_NAME) ||
      !($entity->original instanceof Orejime) ||
      !$entity->hasField('name') ||
      $entity->name->isEmpty() ||
      !$entity->original->hasField('name') ||
      $entity->original->name->isEmpty()
    ) {
      return;
    }

    $name = $entity->name->value;
    $oid = $entity->id();
    $oldName = $entity->original->name->value;
    $oldOid = $entity->original->id();

    $columnName = $name . '_' . $oid;
    $oldColumnName = $oldName . '_' . $oldOid;

    if ($oldColumnName !== $columnName && $schema->fieldExists(self::TABLE_NAME, $oldColumnName)) {
      $schema->changeField(self::TABLE_NAME, $oldColumnName, $columnName, $this->getFieldSpec($name));
    }
  }

  /**
   * Get the field spec customized with the cookies name.
   *
   * @param string $name
   *   The name of the cookie.
   *
   * @return array
   *   The field spec.
   */
  private function getFieldSpec(string $name): array {
    return [
      'description' => "Boolean indicating whether the user accepted $name cookies.",
      'type' => 'int',
      'mysql_type' => 'tinyint',
    ];
  }

}
