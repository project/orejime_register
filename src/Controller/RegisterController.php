<?php

namespace Drupal\orejime_register\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\orejime_register\Form\PurgeByDateForm;
use Drupal\orejime_register\Form\PurgeForm;
use Drupal\orejime_register\Services\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines RegisterController class.
 */
class RegisterController extends ControllerBase {

  /**
   * The database connection used to store entity usage information.
   *
   * @var \Drupal\orejime_register\Services\Database
   */
  protected $database;

  /**
   * RegisterController constructor.
   *
   * @param \Drupal\orejime_register\Services\Database $database
   *   Database link.
   */
  public function __construct(Database $database) {
    $this->database = $database;
    $this->formBuilder();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('orejime_register.database'));
  }

  /**
   * Admin route listing all entries.
   */
  public function list() {
    $list = $this->database->list();
    $names = array_keys($list[0] ?? []);

    foreach ($names as $key => $name) {
      if (in_array($name, ['id', 'created_at'])) {
        unset($names[$key]);
        continue;
      }
      $explodedName = explode('_', $name);
      $oid = array_pop($explodedName);
      $names[$key] = implode('_', $explodedName) . " (id: $oid)";
    }

    return [
      'info' => [
        '#markup' => $this->t('@total elements', ['@total' => count($list)]),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ],
      'table' => [
        '#type' => 'table',
        '#header' => array_merge([
          $this->t('Id'),
          $this->t('Date'),
        ], $names),
        '#rows' => $list,
        '#sticky' => TRUE,
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ];
  }

  /**
   * Admin route used to purge data.
   */
  public function purge(Request $request) {
    return $this->formBuilder->getForm(PurgeForm::class);
  }

  /**
   * Admin route used to purge data by date.
   */
  public function purgeByDate(Request $request) {
    return $this->formBuilder->getForm(PurgeByDateForm::class);
  }

  /**
   * Saves the user choice in the database.
   */
  public function save(Request $request) {
    $this->database->addEntry(json_decode($request->getContent(), TRUE));

    return new Response();
  }

}
