<?php

namespace Drupal\orejime_register\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\orejime_register\Services\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting all registry entries.
 */
class PurgeForm extends ConfirmFormBase {

  /**
   * The database connection used to store entity usage information.
   *
   * @var \Drupal\orejime_register\Services\Database
   */
  protected $database;

  /**
   * RegisterController constructor.
   *
   * @param \Drupal\orejime_register\Services\Database $database
   *   Database link.
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('orejime_register.database'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orejime_registry__purge';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('<p>Are you sure you want to purge registry?</p>');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Orejime register: purge all data');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('orejime_register.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Purge');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->database->purge();

    $this->logger('content')->notice('All data in Orejime registry have been deleted.');
    $this->messenger()->addMessage($this->t('All data in Orejime registry have been deleted.'));
    $form_state->setRedirect('orejime_register.list');
  }

}
