<?php

namespace Drupal\orejime_register\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting all registry entries.
 */
class PurgeByDateForm extends PurgeForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orejime_registry__purge_by_date';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Orejime register: purge by date');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Starting on'),
      '#default_value' => date_create('today')->format('Y-m-d'),
      '#date_format' => 'Y-m-d',
      '#required' => TRUE,
      '#description' => $this->t('Included'),
    ];

    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Until'),
      '#default_value' => date_create('today')->format('Y-m-d'),
      '#date_format' => 'Y-m-d',
      '#required' => TRUE,
      '#description' => $this->t('Included'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $startDate = $form_state->getValue('start_date');
    $endDate = $form_state->getValue('end_date');

    $this->database->purgeByDate(date_create($startDate), date_create($endDate));

    $this->logger('content')->notice(
      'Registry entries have been deleted from the %start_date until the %end_date.',
      ['%start_date' => $startDate, '%end_date' => $endDate]
    );
    $this->messenger()->addMessage($this->t(
      'Registry entries have been deleted from the %start_date until the %end_date.',
      ['%start_date' => $startDate, '%end_date' => $endDate]
    ));
    $form_state->setRedirect('orejime_register.list');
  }

}
