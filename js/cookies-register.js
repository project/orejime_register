/**
 * @file
 * Script used to save cookies user choice to database.
 *
 * Uses Drupal.behaviors.
 *
 * @see https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
 */

(function (Drupal) {
  function hookOrejime() {
    window.orejime.internals.manager.saveAndApplyConsents = function () {
      window.orejime.internals.manager.saveConsents();
      window.orejime.internals.manager.applyConsents();
      fetch('/orejime_register', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(window.orejime.internals.manager.consents),
      });
    };
  }

  Drupal.behaviors.orejimeRegister = {
    attach(context, settings) {
      once('orejimeRegister', 'html', context).forEach(function () {
        if (window.orejime !== undefined) {
          hookOrejime();
        } else {
          Object.defineProperty(window, 'orejime', {
            get() {
              return window.orejime_register;
            },

            set(val) {
              window.orejime_register = val;
              hookOrejime();
            },

            configurable: true,
          });
        }
      });
    },
  };
})(Drupal);
